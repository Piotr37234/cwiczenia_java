package com.javaproject.startapplication;

import javax.persistence.*;

@Entity
class CompanyDepartment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "company_id")
    private Company company;

    @Embedded
    private Address adress;

    CompanyDepartment(final Company company, final String name, final Address address) {
        this.company = company;
        this.name = name;
        this.adress = address;
    }

    CompanyDepartment(final Company company, final String name) {
        this.company = company;
        this.name = name;
    }

    CompanyDepartment() {
        // Required by Hibernate
    }

    public Address getAdress() {
        return adress;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
