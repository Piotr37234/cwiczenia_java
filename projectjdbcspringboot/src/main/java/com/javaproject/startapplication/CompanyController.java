package com.javaproject.startapplication;

import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

@RestController
class CompanyController {

    private static final Pattern COMPANY_VALIDATOR_PATTERN = Pattern.compile("[a-zA-Z ]+");
    private final CompanyRepository companyRepository;

    CompanyController(final CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @GetMapping("/findAllCompanies")
    List<Company> findAllCompanies() {
        return companyRepository.findAll();
    }

    @PostMapping("/addRandomCompany")
    void addRandomCompany() {
        final Company company = new Company(generateCompanyName());
        companyRepository.save(company);
    }

    @Transactional
    @PostMapping("/addCompany")
    void addCompany(@RequestBody final AddCompanyRequest addCompanyRequest) {
        if (!COMPANY_VALIDATOR_PATTERN.matcher(addCompanyRequest.getName()).matches() || addCompanyRequest.getName().length() == 20) {
            throw new IllegalArgumentException();
        }
        final Company company = new Company(addCompanyRequest.getName());
        companyRepository.save(company);
    }

    private String generateCompanyName() {
        final StringBuilder companyName = new StringBuilder();
        final String dictionary = "abcdefghijklmnopqrstuvwxyz0123456789";
        for (int i = 0; i < 20; i++) {
            companyName.append(dictionary.charAt(new Random().nextInt(dictionary.length())));
        }
        return companyName.toString();
    }

    private static class AddCompanyRequest {

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
