package com.javaproject.startapplication;

import org.springframework.data.jpa.repository.JpaRepository;

interface CompanyRepository extends JpaRepository<Company, Long> {

}
