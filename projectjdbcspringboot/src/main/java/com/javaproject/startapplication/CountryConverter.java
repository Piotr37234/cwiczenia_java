package com.javaproject.startapplication;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class CountryConverter implements AttributeConverter<Country, String> {

    @Override
    public String convertToDatabaseColumn(final Country country) {
        if (country == null) {
            return null;
        }
        return country.getCode();
    }

    @Override
    public Country convertToEntityAttribute(final String code) {
        if (code == null) {
            return null;
        }
        return Stream.of(Country.values())
                .filter(c -> c.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
