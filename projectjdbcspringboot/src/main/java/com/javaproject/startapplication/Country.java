package com.javaproject.startapplication;

public enum Country {

    POLAND("PL"),
    UNITED_STATES("US"),
    GREAT_BRITAIN("GB");

    final private String code;

    Country(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}