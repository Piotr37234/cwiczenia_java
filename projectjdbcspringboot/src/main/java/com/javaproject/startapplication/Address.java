package com.javaproject.startapplication;

import javax.persistence.*;

@Embeddable
class Address {

    private String address;
    private String city;
    private Country countryCode;

    public Address(final String address, final String city, final Country countryCode) {
        this.address = address;
        this.city = city;
        this.countryCode = countryCode;
    }

    Address() {
        // Required by Hibernate
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public Country getCountryCode() {
        return countryCode;
    }
}
