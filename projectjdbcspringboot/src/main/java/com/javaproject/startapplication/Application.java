package com.javaproject.startapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Random;

@SpringBootApplication
class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    private final JdbcTemplate jdbcTemplate;

    public Application(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(final String... args) {
        createTable();
        insertFiveRandomClients();
        removeOldestClient();
        displayAllClient();
    }

    private void createTable() {
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS docker_db.client (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(45) NOT NULL, age INT NOT NULL, data datetime NOT NULL, PRIMARY KEY (id))");
    }

    private void insertFiveRandomClients() {
        final String[] names = {"Jan", "Mariusz", "Arek", "Janusz", "Grazyna"};
        for (int i = 0; i < 5; i++) {
            final Timestamp now = new Timestamp(System.currentTimeMillis());
            final int age = new Random().nextInt(82) + 18;
            jdbcTemplate.update("INSERT INTO docker_db.client(name, age, data) values(?,?,?)", names[i], age, now);
        }
    }

    private void removeOldestClient() {
        jdbcTemplate.execute("DELETE FROM docker_db.client WHERE id = (SELECT min(id) FROM (SELECT * FROM docker_db.client) AS x WHERE (SELECT min(data) FROM (SELECT * FROM docker_db.client) AS x)=data)");
    }

    private void displayAllClient() {
        final List<Map<String, Object>> clientList = jdbcTemplate.queryForList("select * from docker_db.client");
        for (final Map<String, Object> row : clientList) {
            log.info("{} {} {} {}", row.get("id"), row.get("name"), row.get("age"), row.get("data"));
        }
    }
}
