alter table company_department
    add address varchar(50) not null;

alter table company_department
    add city varchar(35) not null;

alter table company_department
    add country_code varchar(2) not null;
