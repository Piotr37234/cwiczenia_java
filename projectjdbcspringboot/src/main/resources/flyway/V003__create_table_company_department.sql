create table company_department
(
    id int not null auto_increment,
    name varchar(35) not null,
    company_id int,
    primary key (id),
    foreign key (company_id) references company(id)
);