package com.javaproject.startapplication;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.persistence.*;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.*;

@SpringBootTest
class ApplicationTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private EntityManager em;

    @Autowired
    private TransactionTestService transactionTestService;

    @Autowired
    private CompanyRepository companyRepository;

    @Test
    void testJdbcTransaction() {
        {
            final KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.execute("START TRANSACTION");
            jdbcTemplate.update(connection -> {
                final PreparedStatement ps = connection
                        .prepareStatement("INSERT INTO company(name) values(?)", Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, "FIRMA ABC");
                return ps;
            }, keyHolder);
            jdbcTemplate.execute("COMMIT");

            jdbcTemplate.execute("START TRANSACTION");
            final List<Map<String, Object>> resultList = jdbcTemplate.queryForList("SELECT 1 from company where id = ?", keyHolder.getKey());
            jdbcTemplate.execute("COMMIT");
            Assertions.assertEquals(1, resultList.size());
        }

        {
            final KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.execute("START TRANSACTION");
            jdbcTemplate.update(connection -> {
                final PreparedStatement ps = connection
                        .prepareStatement("INSERT INTO company(name) values(?)", Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, "FIRMA ABC2");
                return ps;
            }, keyHolder);
            jdbcTemplate.execute("ROLLBACK");
            jdbcTemplate.execute("COMMIT");

            jdbcTemplate.execute("START TRANSACTION");
            final List<Map<String, Object>> resultList2 = jdbcTemplate.queryForList("SELECT 1 from company where id = ?", keyHolder.getKey());
            jdbcTemplate.execute("COMMIT");
            Assertions.assertEquals(0, resultList2.size());
        }
    }

    @Test
    void testJpaTransaction() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        {
            entityManager.getTransaction().begin();
            final Company company = new Company("firma ABCD");
            entityManager.persist(company);
            entityManager.getTransaction().commit();

            entityManager.getTransaction().begin();
            Assertions.assertNotNull(entityManager.find(Company.class, company.getId()));
            entityManager.getTransaction().commit();
        }

        {
            entityManager.getTransaction().begin();
            final Company company = new Company("firma ABCDE");
            entityManager.persist(company);
            entityManager.getTransaction().rollback();

            entityManager.getTransaction().begin();
            Assertions.assertNull(entityManager.find(Company.class, company.getId()));
            entityManager.getTransaction().commit();
        }
    }

    @Test
    void testHibernateTransaction() {

        final SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        final Session session = sessionFactory.openSession();
        {
            session.beginTransaction();
            final Company company = new Company("firma ABCDE");
            session.persist(company);
            session.getTransaction().commit();

            session.getTransaction().begin();
            Assertions.assertNotNull(session.find(Company.class, company.getId()));
            session.getTransaction().commit();
        }

        {
            session.beginTransaction();
            final Company company = new Company("firma ABCDEF");
            session.persist(company);
            session.getTransaction().rollback();

            session.getTransaction().begin();
            Assertions.assertNull(session.find(Company.class, company.getId()));
            session.getTransaction().commit();
        }
    }

    @Test
    void testSpringTransaction() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final Long companyId = transactionTestService.insertCompany();
        Assertions.assertNotNull(entityManager.find(Company.class, companyId));

        Assertions.assertThrows(RuntimeException.class, () -> {
            transactionTestService.insertCompany2("xx5");
        });
        final List<Company> resultList = entityManager.createQuery("SELECT c from Company c where c.name=:name", Company.class)
                .setParameter("name", "xx5")
                .getResultList();
        Assertions.assertEquals(0, resultList.size());
    }

    @Test
    void testCompanyDepartment() {
        final Company company = new Company("firma3");
        final Address address1 = new Address("Kwiatowa 38", "Toruń", Country.POLAND);
        final Address address2 = new Address("Kwiatowa 18", "Wrocław", Country.POLAND);
        final CompanyDepartment department1 = new CompanyDepartment(company, "odzial1", address1);
        final CompanyDepartment department2 = new CompanyDepartment(company, "odzial2", address2);

        company.getDepartments().add(department1);
        company.getDepartments().add(department2);
        companyRepository.save(company);

        final Set<Company> resultList = new HashSet<>(em.createQuery("select c from Company c left join fetch c.departments where c.id=:companyId", Company.class)
                .setParameter("companyId", company.getId())
                .getResultList());
        Assertions.assertEquals(1, resultList.size());
        final Set<CompanyDepartment> companyDepartments = resultList.iterator().next().getDepartments();
        Assertions.assertEquals(2, resultList.iterator().next().getDepartments().size());

        for (final Iterator<CompanyDepartment> companyDepartmentIterator = companyDepartments.iterator(); companyDepartmentIterator.hasNext(); ) {
            CompanyDepartment companyDepartment1 = companyDepartmentIterator.next();
            if (companyDepartment1.getId() == department1.getId()) {
                Assertions.assertEquals("odzial1", companyDepartment1.getName());
                Assertions.assertEquals("Toruń", companyDepartment1.getAdress().getCity());
                Assertions.assertEquals("Kwiatowa 38", companyDepartment1.getAdress().getAddress());
            } else if (companyDepartment1.getId() == department2.getId()) {
                Assertions.assertEquals("odzial2", companyDepartment1.getName());
                Assertions.assertEquals("Wrocław", companyDepartment1.getAdress().getCity());
                Assertions.assertEquals("Kwiatowa 18", companyDepartment1.getAdress().getAddress());
                Assertions.assertEquals(Country.POLAND, companyDepartment1.getAdress().getCountryCode());
                Assertions.assertEquals(Country.POLAND, companyDepartment1.getAdress().getCountryCode());
            }
        }
    }

    @Test
    void testEmbeddedClass() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final Company company = new Company("firma AB");
        final Address address = new Address("Kwiatowa 8", "Wrocław", Country.POLAND);
        final CompanyDepartment companyDepartment = new CompanyDepartment(company, "Firma 1", address);
        company.getDepartments().add(companyDepartment);
        companyRepository.save(company);

        final Company company1 = companyRepository.getOne(company.getId());
        Assertions.assertEquals(company.getId(), company1.getId());

        final Set<Company> resultList = new HashSet<>(em.createQuery("select c from Company c left join fetch c.departments where c.id=:companyId", Company.class)
                .setParameter("companyId", company1.getId())
                .getResultList());

        Assertions.assertEquals(1, resultList.size());
        final Set<CompanyDepartment> departments = resultList.iterator().next().getDepartments();

        Assertions.assertEquals(1, departments.size());
        final Address departmentAddress = departments.iterator().next().getAdress();

        Assertions.assertEquals("Kwiatowa 8", departmentAddress.getAddress());
        Assertions.assertEquals("Wrocław", departmentAddress.getCity());
        Assertions.assertEquals(Country.POLAND, departmentAddress.getCountryCode());

        final Query countryCodeQuery = em.createNativeQuery("SELECT country_code from company_department WHERE id=?");
        countryCodeQuery.setParameter(1, companyDepartment.getId());
        Assertions.assertEquals("PL", countryCodeQuery.getSingleResult());
    }
}