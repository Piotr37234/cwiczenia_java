package com.javaproject.startapplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
class TransactionTestService {

    @Autowired
    private CompanyRepository companyRepository;

    @Transactional
    public Long insertCompany() {
        final Company company = companyRepository.save(new Company("Firma ABCD"));
        return company.getId();
    }

    @Transactional
    public Long insertCompany2(final String name) {
        final Company company = companyRepository.save(new Company(name));
        if (true)
            throw new RuntimeException();
        return company.getId();
    }
}
